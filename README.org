* NixOS Laptop

Configuration files for my personal machine, powered by [[https://nixos.org/][NixOS]].

** Modules

The configuration is sliced into different files, per category:

- ZFS pool configuration: hardware-configuration.nix
- Globally installed packages: software.nix
- Network and VPN configuration: networking.nix
- Window manager and userland services: gui.nix
- Synchronization and backup services: datasync.nix
- Sound and music setup: audio.nix
- Development tools: development.nix
- Printing and scanner client: printing.nix
- Systemd user services and timers: periodic.nix
- Power management: power.nix

All the modules are imported in *configuration.nix*
