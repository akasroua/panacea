{
  config,
  lib,
  pkgs,
  ...
}:

{
  # ZFS automatic snapshots
  services.zfs.autoSnapshot = {
    enable = true;
    frequent = 4;
    hourly = 24;
    daily = 7;
    weekly = 4;
    monthly = 12;
  };

  # Syncthing configuration
  services.syncthing = {
    enable = true;
    user = "coolneng";
    dataDir = "/home/coolneng";
    settings = {
      devices.zion = {
        id = "FLI2RS7-GNI5PDM-SQRNF7P-YJIOXJ7-46FRPEI-NRLQGBC-HXRWG7O-RKOVLAF";
        addresses = [ "tcp://192.168.129.2:22000" ];
      };
      folders = {
        Documents = {
          id = "wusdj-bfjkr";
          path = "/home/coolneng/Documents";
          devices = [ "zion" ];
          versioning = {
            type = "simple";
            params.keep = "5";
          };
        };

        Notes = {
          id = "kafhz-bfmzm";
          path = "/home/coolneng/Notes";
          devices = [ "zion" ];
          versioning = {
            type = "simple";
            params.keep = "5";
          };
        };

        Music = {
          id = "2aqt7-vpprc";
          path = "/home/coolneng/Music";
          devices = [ "zion" ];
        };

        Photos = {
          id = "mjibc-ustcg";
          path = "/home/coolneng/Photos";
          devices = [ "zion" ];
        };

        Projects = {
          id = "cjhmu-avy9v";
          path = "/home/coolneng/Projects";
          devices = [ "zion" ];
        };

        Phone = {
          id = "m2007j20cg_vc7r-photos";
          type = "receiveonly";
          path = "/home/coolneng/Photos/Phone";
          devices = [ "zion" ];
        };

        Phone-screenshots = {
          id = "pp70r-pbr70";
          type = "receiveonly";
          path = "/home/coolneng/Photos/Phone-screenshots";
          devices = [ "zion" ];
        };

        Files = {
          id = "tsk52-u6rbk";
          path = "/home/coolneng/Files";
          devices = [ "zion" ];
        };
        Audio = {
          id = "tarrs-5mxck";
          path = "/home/coolneng/Audio";
          devices = [ "zion" ];
        };
      };
    };
  };

}
