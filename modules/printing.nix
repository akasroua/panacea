{
  config,
  lib,
  pkgs,
  ...
}:

{
  # Enable CUPS
  services.printing = {
    enable = true;
    drivers = with pkgs; [
      brgenml1cupswrapper
      hplip
    ];
  };

  # Enable SANE
  hardware.sane = {
    enable = true;
    brscan4.enable = true;
  };
}
