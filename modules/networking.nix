{
  config,
  lib,
  pkgs,
  ...
}:

let
  wireguard_port = "1194";

in
{
  # Set hostname, hostid and enable WiFi
  networking = {
    hostName = "panacea";
    hostId = "8feb0bb8";
    wireless.iwd.enable = true;
  };

  # Enable systemd-networkd
  networking = {
    useDHCP = false;
    interfaces = {
      enp0s31f6.useDHCP = true;
      wlan0.useDHCP = true;
    };
    useNetworkd = true;
    dhcpcd.enable = false;
  };
  systemd.network.wait-online.enable = false;

  # Enable mDNS
  services.resolved = {
    enable = true;
    llmnr = "false";
    extraConfig = ''
      MulticastDNS=yes
    '';
  };

  # Prioritize ethernet over WiFi
  systemd.network.networks."40-enp0s31f6" = {
    dhcpV4Config.RouteMetric = 10;
    networkConfig.MulticastDNS = "yes";
  };
  systemd.network.networks."40-wlan0" = {
    dhcpV4Config.RouteMetric = 20;
    networkConfig.MulticastDNS = "yes";
  };

  # Static IP for home network
  systemd.network.networks."24-home" = {
    name = "wlan0";
    matchConfig = {
      Name = "wlan0";
      SSID = "anakinosi";
    };
    address = [ "192.168.129.3/23" ];
    gateway = [ "192.168.128.1" ];
    dns = [ "192.168.129.2" ];
    networkConfig.MulticastDNS = "yes";
  };

  systemd.network.networks."25-home" = {
    name = "wlan0";
    matchConfig = {
      Name = "wlan0";
      SSID = "Aminkas-5Ghz";
    };
    address = [ "192.168.13.3/24" ];
    gateway = [ "192.168.13.1" ];
    dns = [ "1.1.1.1" ];
    networkConfig.MulticastDNS = "yes";
  };

  # VPN setup
  systemd.network.netdevs."wg0" = {
    netdevConfig = {
      Kind = "wireguard";
      Name = "wg0";
    };
    wireguardConfig = {
      ListenPort = wireguard_port;
      PrivateKeyFile = config.age.secrets.wireguard.path;
      FirewallMark = 34952;
    };
    wireguardPeers = [
      {
        PublicKey = "GN8lqPBZYOulh6xD4GhkoEWI65HMMCpSxJSH5871YnU=";
        AllowedIPs = [ "0.0.0.0/0" ];
        Endpoint = "coolneng.duckdns.org:1194";
      }
    ];
  };
  systemd.network.networks."wg0" = {
    matchConfig.Name = "wg0";
    linkConfig.ActivationPolicy = "manual";
    networkConfig = {
      Address = "10.8.0.2/32";
      DNS = "10.8.0.1";
      DNSDefaultRoute = true;
    };
    routingPolicyRules = [
      {
        FirewallMark = 34952;
        InvertRule = true;
        Table = 1000;
        Priority = 10;
      }
    ];
    routes = [
      {
        Gateway = "10.8.0.1";
        GatewayOnLink = true;
        Table = 1000;
      }
    ];
  };

  # Firewall configuration
  networking.firewall = {
    allowedTCPPorts = [
      9090 # Calibre Wireless
    ];
    allowedUDPPorts = [
      54982 # Calibre Wireless
      5353 # mDNS
    ];
    # Allow wireguard traffic
    extraCommands = ''
      iptables -t mangle -I nixos-fw-rpfilter -p udp -m udp --sport ${wireguard_port} -j RETURN
      iptables -t mangle -I nixos-fw-rpfilter -p udp -m udp --dport ${wireguard_port} -j RETURN
    '';
    extraStopCommands = ''
      iptables -t mangle -D nixos-fw-rpfilter -p udp -m udp --sport ${wireguard_port} -j RETURN || true
      iptables -t mangle -D nixos-fw-rpfilter -p udp -m udp --dport ${wireguard_port} -j RETURN || true
    '';
  };
}
