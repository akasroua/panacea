{
  config,
  lib,
  pkgs,
  ...
}:

with pkgs;

let
  sender = "akasroua@disroot.org";
  recipient = "akasroua+smart@disroot.org";

in
{
  # Notify when a disk starts going haywire
  services.smartd = {
    enable = true;
    defaults.monitored = "-H -f -t -C 197 -U 198 -d nvme";
    autodetect = false;
    devices = [ { device = "/dev/nvme0"; } ];
    notifications.mail = {
      enable = true;
      sender = sender;
      recipient = recipient;
      mailer = "${msmtp}/bin/msmtp -t --read-envelope-from";
    };
  };

  # Notify about zpool problems
  services.zfs.zed = {
    enableMail = false;
    settings = {
      ZED_EMAIL_ADDR = [ "root" ];
      ZED_EMAIL_PROG = "${msmtp}/bin/msmtp";
      ZED_EMAIL_OPTS = "-s '@SUBJECT@' @ADDRESS@";
    };
  };

  # HACK Use an alias to use msmtp instead of the ZED mailer
  environment.etc.aliases.text = ''
    root: ${recipient}
  '';

  # Set up msmtp as notifier
  programs.msmtp = {
    enable = true;
    defaults = {
      auth = true;
      aliases = "/etc/aliases";
      port = 587;
      tls = true;
    };
    accounts = {
      default = {
        host = "disroot.org";
        user = "akasroua@disroot.org";
        passwordeval = "${coreutils}/bin/cat ${config.age.secrets.msmtp.path}";
      };
    };
  };

}
