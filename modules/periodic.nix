{
  config,
  lib,
  pkgs,
  ...
}:

with pkgs;

let
  isync-oauth2 = (isync.override { withCyrusSaslXoauth2 = true; });

in
{
  # Upgrade Doom Emacs daily
  systemd.user.services.doom-upgrade = {
    description = "Upgrade Doom Emacs";
    path = [
      bash
      emacs-vterm
      git
      coreutils
    ];
    script = ''
      ${pkgs.bash}/bin/bash -c "/home/coolneng/.emacs.d/bin/doom -! upgrade"
    '';
    serviceConfig.Type = "oneshot";
    startAt = "22:00:00";
    after = [ "network-online.target" ];
  };

  # Upgrade Neovim plugins weekly
  systemd.user.services.vim-plug-upgrade = {
    description = "Upgrade Vim-Plug";
    path = [
      git
      neovim
    ];
    script = "${pkgs.neovim}/bin/nvim +PlugUpgrade +PlugUpdate +qa";
    serviceConfig.Type = "oneshot";
    startAt = "Wed 18:00:00";
    after = [ "network-online.target" ];
  };

  # Push password-store changes to git daily
  systemd.user.services.password-store-push = {
    description = "Push password-store changes to git";
    path = [
      pass-wayland
      git
      gitAndTools.pass-git-helper
    ];
    script = "${pkgs.pass-wayland}/bin/pass git push";
    serviceConfig.Type = "oneshot";
    startAt = "18:00:00";
    after = [ "network-online.target" ];
  };

  # Sync mail using IDLE
  systemd.user.services.goimapnotify-gmail = {
    description = "Sync gmail mail using IMAP IDLE";
    wantedBy = [ "default.target" ];
    path = [
      goimapnotify
      pass-wayland
      isync-oauth2
      mu
      procps
      emacs-vterm
    ];
    script = ''
      ${pkgs.goimapnotify}/bin/goimapnotify -conf /home/coolneng/.config/goimapnotify/gmail.conf
    '';
    serviceConfig = {
      ExecStartPre = "/home/coolneng/.local/share/scripts/mail-sync gmail";
      Type = "simple";
      Restart = "always";
      RestartSec = 20;
    };
    after = [ "network-online.target" ];
  };

  systemd.user.services.goimapnotify-disroot = {
    description = "Sync disroot mail using IMAP IDLE";
    wantedBy = [ "default.target" ];
    path = [
      goimapnotify
      pass-wayland
      isync-oauth2
      mu
      procps
      emacs-vterm
    ];
    script = ''
      ${pkgs.goimapnotify}/bin/goimapnotify -conf /home/coolneng/.config/goimapnotify/disroot.conf
    '';
    serviceConfig = {
      ExecStartPre = "/home/coolneng/.local/share/scripts/mail-sync disroot";
      Type = "simple";
      Restart = "always";
      RestartSec = 20;
    };
    after = [ "network-online.target" ];
  };

  # Do a full mail sync periodically
  systemd.user.services.periodic-mail-sync = {
    description = "Run a mail sync operation periodically";
    wantedBy = [ "default.target" ];
    path = [
      pass-wayland
      isync-oauth2
      mu
      procps
      emacs-vterm
      python39
      gnupg
      nix
    ];
    script = ''
      /home/coolneng/.local/share/scripts/mail-sync -a
    '';
    after = [ "network-online.target" ];
    startAt = "*-*-* *:00,30:00";
  };

  # HACK Change home partition permissions for mopidy
  systemd.services.chmod-home = {
    description = "Change home partition permissions for Mopidy";
    wantedBy = [ "default.target" ];
    script = "chmod 751 /home/coolneng";
    serviceConfig.Type = "oneshot";
    after = [ "home-coolneng.mount" ];
    before = [
      "mopidy.service"
      "mopidy-scan.service"
    ];
  };

  # Push panacea changes to git daily
  systemd.user.services.panacea-push = {
    description = "Push panacea changes to git";
    path = [
      pass-wayland
      git
      gitAndTools.pass-git-helper
    ];
    script = "${pkgs.git}/bin/git -C /home/coolneng/Projects/panacea push";
    serviceConfig.Type = "oneshot";
    startAt = "14:00:00";
    after = [ "network-online.target" ];
  };

  # Pull changes from zion daily
  systemd.user.services.zion-pull = {
    description = "Pull zion changes to git";
    path = [ git ];
    script = "${pkgs.git}/bin/git -C /home/coolneng/Projects/zion pull";
    serviceConfig.Type = "oneshot";
    startAt = "10:00:00";
    after = [ "network-online.target" ];
  };
}
