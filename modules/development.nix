{
  config,
  lib,
  pkgs,
  ...
}:

{
  # Improve nix-shell and direnv integration
  environment.pathsToLink = [ "/share/nix-direnv" ];
  programs.direnv.enable = true;

  # Set up podman
  virtualisation = {
    containers.enable = true;
    containers.storage.settings.storage = {
      driver = "zfs";
      graphroot = "/var/lib/containers/storage";
      runroot = "/run/containers/storage";
    };

    oci-containers.backend = "podman";

    podman = {
      enable = true;
      dockerCompat = true;
      extraPackages = with pkgs; [ zfs ];
    };
  };

  # Run unpatched binaries with Nix
  programs.nix-ld.enable = true;

  # Embedded development with PlatformIO
  services.udev.packages = with pkgs; [ platformio ];
}
