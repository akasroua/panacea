{
  config,
  lib,
  pkgs,
  inputs,
  ...
}:

with pkgs;

let
  # HACK Replace youtube-dl with yt-dlp in mopidy-youtube
  mopidy-youtube-yt_dlp = mopidy-youtube.overrideAttrs (old: rec {
    propagatedBuildInputs = old.propagatedBuildInputs ++ [
      python3.pkgs.yt-dlp
      python3.pkgs.ytmusicapi
    ];
  });
  soundcloud_token = builtins.readFile ../secrets/soundcloud_token;

in
{
  # Configure pipewire as sound server
  services.pipewire = {
    enable = true;
    alsa = {
      enable = true;
      support32Bit = true;
    };
    pulse.enable = true;
    wireplumber.enable = true;
    # Enable pipewire-pulse's audio via TCP
    extraConfig.pipewire-pulse.tcp-server = {
      "pulse.properties" = {
        "server.address" = [
          "unix:native"
          "tcp:127.0.0.1:4713"
        ];
      };
    };
  };
  security.rtkit.enable = true;

  # Set up Mopidy
  services.mopidy = {
    enable = true;
    extensionPackages = [
      mopidy-mpd
      mopidy-local
      mopidy-youtube-yt_dlp
      mopidy-bandcamp
      mopidy-somafm
      mopidy-soundcloud
    ];
    configuration = ''
      [audio]
      output = pulsesink server=127.0.0.1

      [local]
      media_dir = /home/coolneng/Music
      directories =
        Album Artists       local:directory?type=artist&role=albumartist
        Albums              local:directory?type=album
        Artists             local:directory?type=artist
        Genres              local:directory?type=genre
      excluded_file_extensions =
        .directory
        .html
        .jpeg
        .jpg
        .log
        .nfo
        .png
        .txt
      scan_timeout = 3000

      [mpd]
      zeroconf = ""

      [m3u]
      playlists_dir = /home/coolneng/.config/mpd/playlists
      base_dir = /home/coolneng/Music

      [youtube]
      search_results = 50
      youtube_dl_package = yt_dlp
      musicapi_enabled = true

      [somafm]
      encoding = aac
      quality = highest

      [soundcloud]
      auth_token = ${soundcloud_token}
      explore_songs = 100
    '';
  };
}
