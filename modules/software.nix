{
  config,
  lib,
  pkgs,
  inputs,
  ...
}:

with pkgs;

let
  custom-mpv = (
    mpv-unwrapped.wrapper {
      mpv = mpv-unwrapped;
      scripts = with mpvScripts; [
        sponsorblock-minimal
        mpv-cheatsheet
      ];
    }
  );

  patched-bitwig = bitwig-studio.overrideAttrs (old: rec {
    src = "${inputs.local-bitwig}/bitwig-studio.deb";
    postInstall = ''
      cp -r ${inputs.local-bitwig}/bitwig.jar $out/libexec/bin/bitwig.jar
    '';
  });

in
{
  environment.systemPackages = [
    # Monitoring
    htop
    acpi
    # Password management
    gnupg
    pass-wayland
    passff-host
    gitAndTools.pass-git-helper
    inputs.agenix.packages.${config.nixpkgs.localSystem.system}.default
    git-crypt
    git-lfs
    # Browsers
    ungoogled-chromium
    # LaTeX
    texlive.combined.scheme-full
    pandoc
    texlab
    # Text editors
    neovim
    neovim-remote
    emacs-vterm
    # Emacs dependencies
    (ripgrep.override { withPCRE2 = true; })
    fd
    clang
    coreutils
    # Terminals
    tmux
    tmuxp
    fish
    # Development
    git
    direnv
    nix-direnv
    gnumake
    gitAndTools.pre-commit
    # Rice
    adapta-gtk-theme
    paper-icon-theme
    # Audio
    mopidy
    ncmpcpp
    mpc_cli
    pulsemixer
    easyeffects
    patched-bitwig
    # Productivity
    zathura
    libreoffice-fresh
    simple-scan
    bc
    pdfgrep
    # Mail stack
    mu
    (isync.override { withCyrusSaslXoauth2 = true; })
    msmtp
    # Media
    gimp
    custom-mpv
    nodePackages.webtorrent-cli
    deluge
    yt-dlp-light
    ffmpeg
    calibre
    beets
    # File management
    zip
    unzip
    unar
    # Overlays
    cachix
    # Budgeting
    beancount
    beancount-language-server
    fava
    # Programming tools
    ## Shell
    shellcheck
    shfmt
    ## Nix
    nixfmt-rfc-style
    ## Python
    basedpyright
    black
    ## C/C++
    clang-tools
    ## Go
    gopls
    gotests
    gore
    golangci-lint
  ];

  # Fonts declaration
  fonts.packages = with pkgs; [
    google-fonts
    terminus_font_ttf
    iosevka-bin
    libertine
    nerd-fonts.inconsolata
    nerd-fonts.symbols-only
  ];

}
