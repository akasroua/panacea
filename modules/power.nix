{
  config,
  lib,
  pkgs,
  ...
}:

{
  # Enable power-profiles-daemon
  services.power-profiles-daemon.enable = true;

  # Suspend when the battery is critical
  services.udev.extraRules = ''
    SUBSYSTEM=="power_supply", ATTR{status}=="Discharging", ATTR{model_name}=="01AV405", ATTR{capacity}=="[0-5]", RUN+="${config.systemd.package}/bin/systemctl suspend -i"
  '';
}
