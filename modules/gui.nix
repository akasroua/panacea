{
  config,
  lib,
  pkgs,
  ...
}:

with pkgs;

{
  # Display manager
  services.xserver.enable = true;
  services.xserver.displayManager = {
    defaultSession = "sway";
    autoLogin = {
      enable = true;
      user = "coolneng";
    };
    lightdm = {
      enable = true;
      greeter.enable = false;
    };
  };

  # Window manager
  programs.sway = {
    enable = true;
    extraPackages = [
      xwayland
      swaylock
      swayidle
      swaybg
      rofi-wayland
      waybar
      clipman
      wl-clipboard
      grim
      slurp
      swayimg
      kanshi
      kitty
      qt5.qtwayland
      glib
    ];
    extraSessionCommands = ''
      export MOZ_ENABLE_WAYLAND=1
      export MOZ_USE_XINPUT2=1
      export GRIM_DEFAULT_DIR=/home/coolneng/Photos/Screenshots
      export _JAVA_AWT_WM_NONREPARENTING=1
      export SDL_VIDEODRIVER=wayland
      export XDG_SESSION_TYPE=wayland
      export XDG_CURRENT_DESKTOP=sway
      export NVIM_LISTEN_ADDRESS=/tmp/nvimsocket
      export GIT_CONFIG_NOSYSTEM=true
      export SYSTEMD_LESS=FRSMK
    '';
    wrapperFeatures.gtk = true;
  };

  # Backlight
  programs.light.enable = true;

  # Blue light filter
  location.provider = "geoclue2";

  services.redshift = {
    enable = true;
    package = gammastep;
    executable = "/bin/gammastep";
    temperature = {
      day = 6500;
      night = 5300;
    };
  };

  # GPG agent
  programs.gnupg.agent = {
    enable = true;
    pinentryPackage = pinentry-gnome3;
  };

  # Tmux
  programs.tmux = {
    enable = true;
    historyLimit = 5000;
    keyMode = "vi";
    shortcut = "x";
    terminal = "screen-256";
    baseIndex = 1;
  };

  # Integrate pass with the browser
  programs.browserpass.enable = true;
  programs.firefox.enable = true;

  # Enable WebRTC screensharing
  xdg.portal = {
    enable = true;
    wlr.enable = true;
  };

  # Allow dbus to communicate with storage devices
  services.udisks2.enable = true;
}
