{
  description = "System configuration for panacea";

  nixConfig = {
    extra-substituters = "https://cachix.cachix.org https://nix-community.cachix.org";
    extra-trusted-public-keys = ''
      cachix.cachix.org-1:eWNHQldwUO7G2VkjpnjDbWwy4KQ/HNxht7H4SSoMckM=
            nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs='';
  };

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    nixos-hardware.url = "github:NixOS/nixos-hardware";
    agenix = {
      url = "github:ryantm/agenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    cyrus-sasl-xoauth2 = {
      url = "github:robn/sasl2-oauth";
      flake = false;
    };
    nix-index-database = {
      url = "github:Mic92/nix-index-database";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    lanzaboote = {
      url = "github:nix-community/lanzaboote/v0.4.2";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    local-bitwig = {
      url = "path:/home/coolneng/Projects/panacea/assets/bitwig";
      flake = false;
    };
  };

  outputs =
    { self, nixpkgs, ... }@inputs:
    let
      system = "x86_64-linux";

      pkgs = import nixpkgs {
        inherit system;
        config.allowUnfree = true;
        overlays = [
          (final: prev: {
            emacs-vterm = (
              (pkgs.emacsPackagesFor pkgs.emacs29-pgtk).emacsWithPackages (
                epkgs: with epkgs; [
                  vterm
                  mu4e
                ]
              )
            );
          })
        ];
      };

      lib = nixpkgs.lib;

    in
    {
      nixosConfigurations.panacea = lib.nixosSystem {
        inherit system;
        modules = [
          (import ./configuration.nix)
          inputs.nixos-hardware.nixosModules.lenovo-thinkpad-e14-amd
          inputs.agenix.nixosModules.age
          inputs.nix-index-database.nixosModules.nix-index
          inputs.lanzaboote.nixosModules.lanzaboote
        ];
        specialArgs = {
          inherit inputs;
          inherit pkgs;
        };
      };

    };
}
