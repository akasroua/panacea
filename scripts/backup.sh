#!/bin/sh

wifi_backup() {
    zip /tmp/iwd-networks.zip /var/lib/iwd
    scp /tmp/iwd-networks.zip zion:/vault/backups/panacea/iwd
}

wifi_backup
