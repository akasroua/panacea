#!/bin/sh

partition_disk() {
    parted "$DISK" -- mklabel gpt
    parted "$DISK" -- mkpart ESP fat32 1MiB 512MiB
    parted "$DISK" -- mkpart primary 512MiB 100%
    parted "$DISK" -- set 1 boot on
    mkfs.fat -F32 -n BOOT "$DISK"p1
}

zfs_setup() {
    zpool create -f -o ashift=13 -o autotrim=on -O acltype=posixacl -O relatime=on \
        -O xattr=sa -O dnodesize=legacy -O normalization=formD -O mountpoint=none \
        -O canmount=off -O devices=off -R /mnt -O compression=zstd -O encryption=aes-256-gcm \
        -O keyformat=passphrase -O keylocation=prompt syscea "$DISK"p2
    zfs create -o mountpoint=legacy -o com.sun:auto-snapshot=false syscea/ephemeral
    zfs create -o mountpoint=legacy -o com.sun:auto-snapshot=false syscea/ephemeral/nix
    zfs create -o mountpoint=legacy -o com.sun:auto-snapshot=false -o sync=disabled -o setuid=off syscea/ephemeral/tmp
    zfs create -o mountpoint=legacy -o com.sun:auto-snapshot=false syscea/stateful
    zfs create -o mountpoint=legacy -o com.sun:auto-snapshot=true syscea/stateful/home
    zfs create -o mountpoint=legacy -o com.sun:auto-snapshot=false syscea/stateful/home/downloads
    zfs create -o mountpoint=legacy -o com.sun:auto-snapshot=false syscea/stateful/root
    zfs create -V 6G -b "$(getconf PAGESIZE)" -o compression=zle -o logbias=throughput -o sync=always \
        -o primarycache=metadata -o secondarycache=none -o com.sun:auto-snapshot=false syscea/ephemeral/swap
    mkswap -f /dev/zvol/syscea/ephemeral/swap && swapon /dev/zvol/syscea/ephemeral/swap
}

mount_datasets() {
    mount -t zfs syscea/stateful/root /mnt
    mkdir -p /mnt/boot
    mount "$DISK"p1 /mnt/boot
    mkdir -p /mnt/home/coolneng
    mount -t zfs syscea/stateful/home /mnt/home/coolneng
    mkdir -p /mnt/home/coolneng/Downloads
    mount -t zfs syscea/stateful/home/downloads /mnt/home/coolneng/Downloads
    mkdir -p /mnt/nix
    mount -t zfs syscea/ephemeral/nix /mnt/nix
    mkdir -p /mnt/tmp
    mount -t zfs syscea/ephemeral/tmp /mnt/tmp
}

install_system() {
    nixos-generate-config --root /mnt
    sed -i "s/\${soundcloud_token}/PLACEHOLDER/" modules/audio.nix
    mv /mnt/etc/nixos/hardware-configuration.nix modules/hardware-configuration.nix
    nix-shell -p git nixFlakes --command "nixos-install --root /mnt --flake .#panacea"
}

usage() {
    echo "Usage: install.sh <disk>"
    echo "disk: full path to the disk (e.g. /dev/sda)"
    exit 1
}

if [ $# != 1 ]; then
    usage
fi

DISK="$1"

echo "Let's start by partitioning the disk"
partition_disk
echo "Starting up the ZFS machinery"
zfs_setup
echo "Mounting the horse"
mount_datasets
echo "Lift off to the NixOS planet"
install_system
echo "All ready, time to rejoice"
