switch:
	sudo nixos-rebuild switch --flake path://$(PWD)#

update:
	nix flake update --commit-lock-file

upgrade:
	make update && make switch

install:
	./scripts/install.sh

backup:
	./scripts/backup.sh

.DEFAULT_GOAL := switch
