{
  config,
  lib,
  pkgs,
  inputs,
  ...
}:

with pkgs;

{
  # Kernel configuration
  boot = {
    blacklistedKernelModules = [
      "btusb"
      "bluetooth"
    ];
    kernelParams = [
      "zfs.zfs_arc_max=8589934592"
      "zfs.zfs_arc_min=1073741824"
    ];
    supportedFilesystems = [ "zfs" ];
    zfs = {
      requestEncryptionCredentials = true;
      package = zfs_unstable;
    };
  };

  # Device firmware updates
  services.fwupd.enable = true;

  # Secure boot using lanzaboote
  boot.loader = {
    efi.canTouchEfiVariables = true;
    systemd-boot = {
      enable = false;
      configurationLimit = 50;
      editor = false;
    };
    timeout = 3;
  };
  boot.lanzaboote = {
    enable = true;
    pkiBundle = "/etc/secureboot";
  };

  # Run Nix garbage collector and enable flakes
  nix = {
    settings = {
      auto-optimise-store = true;
      trusted-users = [
        "root"
        "coolneng"
      ];
    };
    gc = {
      automatic = true;
      options = "--delete-older-than 7d";
      dates = "Tue 23:00";
    };
    extraOptions = ''
      keep-outputs = true
      keep-derivations = true
      gc-keep-outputs = true
      experimental-features = nix-command flakes
    '';
  };

  # Clean tmp directory on shutdown
  boot.tmp.cleanOnBoot = true;

  # Keep logs for a month
  services.journald.extraConfig = "MaxRetentionSec=4week";

  # Scrub zpool monthly
  services.zfs.autoScrub = {
    enable = true;
    interval = "monthly";
  };

  # Set timezone and synchronize NTP
  time.timeZone = "Europe/Brussels";
  services.chrony.enable = true;

  # NixOS version
  system.stateVersion = "24.05";

  # Create coolneng user
  users.users.coolneng = {
    isNormalUser = true;
    home = "/home/coolneng";
    extraGroups = [
      "wheel"
      "video"
      "audio"
      "lp"
    ];
    shell = fish;
  };

  # Set shell for root user
  users.users.root.shell = fish;

  # Specify secrets
  age = {
    secrets.wireguard = {
      file = secrets/wireguard.age;
      owner = "systemd-network";
      group = "systemd-network";
    };
    secrets.syncthing.file = secrets/syncthing.age;
    secrets.msmtp.file = secrets/msmtp.age;
    identityPaths = [ "/etc/ssh/id_ed25519" ];
  };

  # Use same version of nixpkgs for nix-shell
  nix.nixPath =
    let
      path = toString ./.;
    in
    [
      "nixpkgs=${inputs.nixpkgs}"
      "nixos-config=${path}/configuration.nix"
    ];

  # Auto-upgrade the system
  system.autoUpgrade = {
    enable = true;
    dates = "22:30";
    flake = "/home/coolneng/Projects/panacea";
    flags = [
      "--update-input"
      "nixpkgs"
      "--commit-lock-file"
    ];
  };

  # Add required dependencies to the auto-upgrade service
  systemd.services.nixos-upgrade.path = [
    coreutils
    gnutar
    xz.bin
    gzip
    gitMinimal
    config.nix.package.out
    config.programs.ssh.package
    git-crypt
    git-lfs
    inputs.agenix.packages.${config.nixpkgs.localSystem.system}.default
  ];

  # Configure git for auto-upgrade
  programs.git = {
    enable = true;
    config = {
      user.name = "coolneng";
      user.email = "akasroua@gmail.com";
      safe.directory = "/home/coolneng/Projects/panacea";
    };
  };

  # Enable fish package completion
  programs.fish.enable = true;

  # Enable nix-index
  programs.command-not-found.enable = false;
  programs.nix-index = {
    enable = true;
    enableFishIntegration = true;
  };

  # Import other configuration modules
  imports = [
    ./modules/hardware-configuration.nix
    ./modules/software.nix
    ./modules/networking.nix
    ./modules/gui.nix
    ./modules/datasync.nix
    ./modules/audio.nix
    ./modules/development.nix
    ./modules/printing.nix
    ./modules/periodic.nix
    ./modules/power.nix
    ./modules/monitoring.nix
    ./modules/latest-zfs-kernel.nix
  ];

}
