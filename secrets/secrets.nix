let
  coolneng = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIC57m1j/G6iQyi2EpU3nj3+df5Z4PL/XbiOmDcqA7ODg";
in
{
  "wireguard.age".publicKeys = [ coolneng ];
  "syncthing.age".publicKeys = [ coolneng ];
  "msmtp.age".publicKeys = [ coolneng ];
}
